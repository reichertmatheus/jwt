package com.jwtme.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@RequestMapping("/users")
	@ResponseBody
	public String getUsers() {
		return "{\"users\":[{\"nome\":\"Matheus\", \"sobrenome\":\"Reichert\"}," +
		           "{\"nome\":\"Jacson\",\"sobrenome\":\"Polo\"}]}";
	}
}
